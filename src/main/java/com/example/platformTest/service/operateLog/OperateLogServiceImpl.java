package com.example.platformTest.service.operateLog;

import com.example.platformTest.dao.LogMapper;
import com.example.platformTest.entity.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: upsmart
 * @Description:
 * @Date: Created by 下午12:11 on 17-11-15.
 * @Modified By:
 */

@Service
public class OperateLogServiceImpl implements OperateLogService{

    @Autowired
    private LogMapper logMapper;

    /**
     * @desc 通过公司id查找记录
     * @return List<Log>
     */
    @Override
    public List<Log> selectByComid(int comid) {

        List<Log> logs = logMapper.selectByComid(comid);
        return logs;
    }
}
