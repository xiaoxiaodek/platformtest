package com.example.platformTest.dao;

import com.example.platformTest.entity.Contract;

import java.util.List;

public interface ContractMapper {
    int deleteByPrimaryKey(Integer cid);

    int insert(Contract record);

    int insertSelective(Contract record);

    Contract selectByPrimaryKey(Integer cid);

    List<Contract> selectAll();

    List<Contract> selectByCompany(Integer comId);

    int updateByPrimaryKeySelective(Contract record);

    int updateByPrimaryKey(Contract record);
}