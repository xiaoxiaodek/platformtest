package com.example.platformTest.dao;

import com.example.platformTest.entity.Intfaceexinf;

public interface IntfaceexinfMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Intfaceexinf record);

    int insertSelective(Intfaceexinf record);

    Intfaceexinf selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Intfaceexinf record);

    int updateByPrimaryKey(Intfaceexinf record);
}